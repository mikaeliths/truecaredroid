package com.parse.starter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class RegisterActivity extends AppCompatActivity {

    private EditText usernameEdit, passwordEdit;
    private Button registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        usernameEdit = (EditText) findViewById(R.id.edittext_register_username);
        passwordEdit = (EditText) findViewById(R.id.edittext_register_password);

        registerButton = (Button) findViewById(R.id.button_register_register);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getBaseContext(), usernameEdit.getText() + " " + passwordEdit.getText(), Toast.LENGTH_SHORT).show();
                ParseUser user = new ParseUser();
                user.setUsername(usernameEdit.getText().toString());
                user.setEmail(usernameEdit.getText().toString()); //username and email is same
                user.setPassword(passwordEdit.getText().toString());
                user.put("employer", false); //user cannot register as employer yet

                user.signUpInBackground(new SignUpCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            // Hooray! Let them use the app now.
                            Log.d("tag", "Signup success");
                            Toast.makeText(getBaseContext(), "Signup success!",Toast.LENGTH_SHORT).show();
                            setResult(RESULT_OK);
                            finish();
                        } else {
                            // Sign up didn't succeed. Look at the ParseException
                            // to figure out what went wrong
                            Log.d("tag", "Signup failed: "+e);
                            Toast.makeText(getBaseContext(), "Signup failed: "+e.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }
}
