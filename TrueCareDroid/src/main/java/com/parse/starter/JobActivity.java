package com.parse.starter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.parse.ParseQuery;

public class JobActivity extends AppCompatActivity {

    private TextView jobText, startText, endText, payText, companyText, descriptionText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job);

        jobText = (TextView) findViewById(R.id.text_job_job);
        startText = (TextView) findViewById(R.id.text_job_start);
        endText = (TextView) findViewById(R.id.text_job_end);
        payText = (TextView) findViewById(R.id.text_job_pay);
        companyText = (TextView) findViewById(R.id.text_job_company);
        descriptionText = (TextView) findViewById(R.id.text_job_description);

        Intent i = getIntent();
        String Id = i.getStringExtra("ID");

        jobText.setText(Id);

    }
}
