package com.parse.starter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

public class ProfileActivity extends AppCompatActivity {

    private EditText firstNameEdit, lastNameEdit, emailEdit, phoneEdit, companyEdit, genderEdit, ageEdit;
    private Button professionsButton, doneButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        firstNameEdit = (EditText) findViewById(R.id.edit_profile_first_name);
        lastNameEdit = (EditText) findViewById(R.id.edit_profile_last_name);
        emailEdit = (EditText) findViewById(R.id.edit_profile_email);
        phoneEdit = (EditText) findViewById(R.id.edit_profile_telephone);
        companyEdit = (EditText) findViewById(R.id.edit_profile_company);
        genderEdit = (EditText) findViewById(R.id.edit_profile_gender);
        ageEdit = (EditText) findViewById(R.id.edit_profile_age);

        professionsButton = (Button) findViewById(R.id.button_profile_professions);
        doneButton = (Button) findViewById(R.id.button_profile_done);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_profile);

        setSupportActionBar(toolbar);

        ParseUser currentUser = ParseUser.getCurrentUser();

        if (currentUser.getString("firstName") != null) {
            firstNameEdit.setText(currentUser.getString("firstName"));
        }
        if (currentUser.getString("lastName") != null) {
            lastNameEdit.setText(currentUser.getString("lastName"));
        }
        if (currentUser.getString("email") != null) {
            emailEdit.setText(currentUser.getString("email"));
        }
        if (currentUser.getString("tel") != null) {
            phoneEdit.setText(currentUser.getString("tel"));
        }
        if (currentUser.getString("company") != null) {
            companyEdit.setText(currentUser.getString("company"));
        }
        if (currentUser.getString("gender") != null) {
            genderEdit.setText(currentUser.getString("gender"));
        }
        if (currentUser.getString("age") != null) {
            ageEdit.setText(currentUser.getString("age"));
        }

        professionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "button clicked", Toast.LENGTH_SHORT).show();
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseUser currentUser = ParseUser.getCurrentUser();

                currentUser.put("firstName", firstNameEdit.getText().toString());
                currentUser.put("lastName", lastNameEdit.getText().toString());
                currentUser.put("email", emailEdit.getText().toString());
                currentUser.put("tel", phoneEdit.getText().toString());
                currentUser.put("company", companyEdit.getText().toString());
                currentUser.put("gender", genderEdit.getText().toString());
                currentUser.put("age", ageEdit.getText().toString());

                currentUser.saveInBackground();

                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(intent);
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            ParseUser.logOutInBackground(new LogOutCallback() {
                @Override
                public void done(ParseException e) {
                    ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                    installation.remove("user");
                    installation.saveInBackground();
                    /*Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);*/
                    finish();
                }
            });

        }

        return super.onOptionsItemSelected(item);
    }
}
