package com.parse.starter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class LoginActivity extends AppCompatActivity {

    private EditText usernameEdit, passwordEdit;
    private Button loginButton, forgotButton, registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameEdit = (EditText) findViewById(R.id.edittext_login_username);
        passwordEdit = (EditText) findViewById(R.id.edittext_login_password);

        loginButton = (Button) findViewById(R.id.button_login_login);
        forgotButton = (Button) findViewById(R.id.button_login_forgot);
        registerButton = (Button) findViewById(R.id.button_login_register);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), RegisterActivity.class);
                startActivityForResult(intent, 0);
            }
        });

        forgotButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getBaseContext(), usernameEdit.getText()+" "+passwordEdit.getText(), Toast.LENGTH_SHORT).show();
                ParseUser.logInInBackground(usernameEdit.getText().toString(), passwordEdit.getText().toString(), new LogInCallback() {
                    @Override
                    public void done(ParseUser user, ParseException e) {
                        if (user != null) {
                            // Hooray! The user is logged in.
                            ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                            installation.put("user", user);
                            installation.saveInBackground();

                            if (user.getBoolean("employer")) {
                                Log.d("tag", "Login success, is employer");
                                Toast.makeText(getBaseContext(), "Login success, employer!",Toast.LENGTH_SHORT).show();
                                goToEmployerActivity();
                            } else {
                                Log.d("tag", "Login success");
                                Toast.makeText(getBaseContext(), "Login success!",Toast.LENGTH_SHORT).show();
                                goToMainActivity();
                            }
                        } else {
                            // Signup failed. Look at the ParseException to see what happened.
                            Log.d("tag", "Login failed: "+e);
                            Toast.makeText(getBaseContext(), "Login failed: "+e.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                goToMainActivity();
            }
        }
    }

    private void goToMainActivity() {
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(intent);
    }

    private void goToEmployerActivity() {
        Intent intent = new Intent(getBaseContext(), EmployerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(intent);
    }
}
