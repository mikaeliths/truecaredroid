package com.parse.starter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

public class ForgotPasswordActivity extends AppCompatActivity {

    private EditText usernameEdit;
    private Button sendButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        usernameEdit = (EditText) findViewById(R.id.edittext_forgot_username);
        sendButton = (Button) findViewById(R.id.button_forgot_send);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseUser.requestPasswordResetInBackground(usernameEdit.getText().toString(), new RequestPasswordResetCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            // An email was successfully sent with reset instructions.
                            Log.d("tag", "Password reset success");
                            Toast.makeText(getBaseContext(), "E-post skickat!", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            // Something went wrong. Look at the ParseException to see what's up.
                            Log.d("tag", "Password reset failed: "+e);
                            Toast.makeText(getBaseContext(), "Kunde inte skicka: "+e.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

    }
}
